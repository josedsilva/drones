import {fleet} from './fleet-data.js';
import {FleetDataService} from './services/fleet-data-service.js';

let dataService = new FleetDataService();
dataService.loadData(fleet);

// TEST: log license numbers of cars in the fleet (to check if everything is working correctly)
/*for (let car of dataService.cars) {
  console.log(car.license);
}*/

/*let license = 'TY9876';
console.log(`car with license: ${license}`, dataService.getCarByLicense(license));*/

let cars = dataService.getCarsSortedByLicense();
for (let {license} of cars) {
  console.log(license);
}